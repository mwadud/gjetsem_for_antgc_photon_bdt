import FWCore.ParameterSet.Config as cms

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

# inspirations from:
# https://cms-pdmv.cern.ch/mcm/public/restapi/requests/get_fragment/EGM-RunIISummer17GS-00014
# https://raw.githubusercontent.com/cms-sw/genproductions/a10c4661b08ff764b90a8b1947689c52c5c992fb/python/ThirteenTeV/Hadronizer/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py
# https://cms-pdmv.cern.ch/mcm/public/restapi/requests/get_fragment/EXO-RunIIFall17wmLHEGS-00003

# https://monte-carlo-production-tools.gitbook.io

# https://github.com/cms-sw/cmssw/blob/master/GeneratorInterface/Pythia8Interface/plugins/Pythia8Hadronizer.cc
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideAboutPythonConfigFile
# https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/GeneratorInterface/Pythia8Interface/src/Py8InterfaceBase.cc

# https://twiki.cern.ch/twiki/bin/view/Main/ExoMCInstructions#Multi_year_requests
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookChapter6#RndmSeeds
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCmsDriver
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideSimulation#Rerun_a_job_with_the_same_random
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/MCproduction
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookGeneration#RndmSeeds

# source = cms.Source("EmptySource")

generator = cms.EDFilter('Pythia8GeneratorFilter',
						 comEnergy=cms.double(13000.0),
						 crossSection=cms.untracked.double(1.0),
						 filterEfficiency=cms.untracked.double(1.0),
						 maxEventsToPrint=cms.untracked.int32(0),
						 pythiaHepMCVerbosity=cms.untracked.bool(True),
						 pythiaPylistVerbosity=cms.untracked.int32(True),
						 # do MC reweighting suitable for CP5 tune :
						 # https://github.com/cms-sw/cmssw/pull/24179
						 # https://indico.cern.ch/event/748684/contributions/3096602/attachments/1697999/2733508/pthat.pdf
						 reweightGenEmp=cms.PSet(
							 tune=cms.string('CP5')),
						 PythiaParameters=cms.PSet(
							 pythia8CommonSettingsBlock,
							 pythia8CP5SettingsBlock,
							 pythia8PSweightsSettingsBlock,
							 processParameters=cms.vstring(
								 # http://home.thep.lu.se/~torbjorn/pythia82html/ElectroweakProcesses.html
								 'PromptPhoton:qg2qgamma = on       ! prompt photon production',
								 'PromptPhoton:qqbar2ggamma = on    ! prompt photon production',
								 'PromptPhoton:gg2ggamma = on       ! prompt photon production',
								 'PhaseSpace:pTHatMin = 20.      	! minimum pt hat for hard interactions',
								 'PhaseSpace:pTHatMax = -1          ! maximum pt hat for hard interactions',
								 # https://cmssdt.cern.ch/lxr/source/GeneratorInterface/Pythia8Interface/src/Py8InterfaceBase.cc?%21v=CMSSW_9_1_1
								 # https://github.com/cms-sw/cmssw/blob/982c595daa9fdde39b7a5925f706b457b178607d/GeneratorInterface/Pythia8Interface/src/PTFilterHook.cc
								 # 'PTFilter:filter = on',
								 # 'PTFilter:scaleToFilter = 1.',
								 # 'PTFilter:quarkToFilter = 22',
								 # 'PTFilter:quarkPt = 190.',
								 # 'PTFilter:quarkRapidity = 1.48'
							 ),
							 parameterSets=cms.vstring('pythia8CommonSettings',
													   'pythia8CP5Settings',
													   'pythia8PSweightsSettings',
													   'processParameters')
						 )
						 )


# photon pT filter
# https://github.com/ahlinist/cmssw/blob/master/Configuration/CSA06Skimming/src/MCSingleParticleFilter.cc
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideGenFilters#2_Filter_based_on_the_MC_particl
# gj_filter = cms.EDFilter("MCSingleParticleFilter",
# 						   ParticleID=cms.untracked.vint32(22),
# 						   Status=cms.untracked.vint32(1),
# 						   MaxEta=cms.untracked.vdouble(1.48),  # barrel only
# 						   MinEta=cms.untracked.vdouble(-1.48),  # barrel only
# 						   MinPt=cms.untracked.vdouble(190.))

# photon pT filter
# https://github.com/cms-sw/cmssw/blob/c60e9e428c56fe3e03de493256a0b8e842173348/GeneratorInterface/GenFilters/plugins/PythiaFilter.cc
# https://github.com/cms-sw/cmssw/blob/c60e9e428c56fe3e03de493256a0b8e842173348/GeneratorInterface/GenFilters/plugins/PythiaFilterGammaJet.cc
pho_filter = cms.EDFilter("PythiaFilter",
						  ParticleID=cms.untracked.int32(22),
						  Status=cms.untracked.int32(23),
						  MaxEta=cms.untracked.double(2.52),
						  MinEta=cms.untracked.double(-2.52),
						  MinPt=cms.untracked.double(180.),
						  MaxPt=cms.untracked.double(1200.))

# https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/GeneratorInterface/GenFilters/python
# https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/GeneratorInterface/GenFilters/src/doubleEMEnrichingFilterAlgo.cc
# https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/GeneratorInterface/GenFilters/src/PythiaFilterGammaGamma.cc
# gj_filter = cms.EDFilter("PythiaFilterGammaGamma",
#                          PtSeedThr=cms.untracked.double(5.0),
#                          EtaSeedThr=cms.untracked.double(2.8),
#                          PtGammaThr=cms.untracked.double(180.0),
#                          EtaGammaThr=cms.untracked.double(1.48),
#                          PtElThr=cms.untracked.double(2.0),
#                          EtaElThr=cms.untracked.double(2.8),
#                          dRSeedMax=cms.untracked.double(0.0),
#                          dPhiSeedMax=cms.untracked.double(0.2),
#                          dEtaSeedMax=cms.untracked.double(0.12),
#                          dRNarrowCone=cms.untracked.double(0.02),
#                          PtTkThr=cms.untracked.double(1.6),
#                          EtaTkThr=cms.untracked.double(2.2),
#                          dRTkMax=cms.untracked.double(0.2),
#                          PtMinCandidate1=cms.untracked.double(180.),
#                          PtMinCandidate2=cms.untracked.double(15.0),
#                          EtaMaxCandidate=cms.untracked.double(3.),
#                          NTkConeMax=cms.untracked.int32(50),
#                          NTkConeSum=cms.untracked.int32(100),
#                          InvMassMin=cms.untracked.double(0.0),
#                          InvMassMax=cms.untracked.double(13000.0),
#                          EnergyCut=cms.untracked.double(1.),
#                          AcceptPrompts=cms.untracked.bool(True),
#                          PromptPtThreshold=cms.untracked.double(15.0)
#                          )

gj_filter = cms.EDFilter("PythiaFilterGammaGamma",
						 PtSeedThr=cms.untracked.double(5.0),
						 EtaSeedThr=cms.untracked.double(2.8),
						 PtGammaThr=cms.untracked.double(0.0),
						 EtaGammaThr=cms.untracked.double(2.8),
						 PtElThr=cms.untracked.double(2.0),
						 EtaElThr=cms.untracked.double(2.8),
						 dRSeedMax=cms.untracked.double(0.0),
						 dPhiSeedMax=cms.untracked.double(0.2),
						 dEtaSeedMax=cms.untracked.double(0.12),
						 dRNarrowCone=cms.untracked.double(0.02),
						 PtTkThr=cms.untracked.double(1.6),
						 EtaTkThr=cms.untracked.double(2.2),
						 dRTkMax=cms.untracked.double(0.2),
						 PtMinCandidate1=cms.untracked.double(15.),
						 PtMinCandidate2=cms.untracked.double(15.),
						 EtaMaxCandidate=cms.untracked.double(3.0),
						 NTkConeMax=cms.untracked.int32(2),
						 NTkConeSum=cms.untracked.int32(4),
						 InvMassMin=cms.untracked.double(40.0),
						 InvMassMax=cms.untracked.double(13000.0),
						 EnergyCut=cms.untracked.double(1.0),
						 AcceptPrompts=cms.untracked.bool(True),
						 PromptPtThreshold=cms.untracked.double(15.0)
						 )

ProductionFilterSequence = cms.Sequence(generator * pho_filter * gj_filter)
