#!/bin/bash

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo $current_date_time

source /cvmfs/cms.cern.ch/cmsset_default.sh;
eval `scramv1 runtime -sh`

#resources
#https://twiki.cern.ch/twiki/bin/view/CMS/PdmV2017Analysis?rev=209
#https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCmsDriver?rev=11


nThreads1=4
nThreads2=$nThreads1

GT=94X_mc2017_realistic_v17
beamspotConditions=Realistic25ns13TeVEarly2017Collision

# pileupDataset=file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00010/AA305EE3-5A0D-E811-B613-0CC47A4C8ECE.root
# gensimfile=gensimfile_PythiaFilterGammaGamma.root
# miniaodsimfile=miniaodsimfile_PythiaFilterGammaGamma.root

nEvents=12345
pileupDataset=puFile
gensimfile=gensimfile
miniaodsimfile=miniaodsimfile


echo "Generating fragments for producing simulation of Gamma+Jets..."
current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo $current_date_time

cmsDriver.py aNTGC_GJets.py --nThreads ${nThreads1} --mc --step GEN,SIM,DIGIPREMIX_S2,DATAMIX,L1,DIGI2RAW,HLT -n ${nEvents} --conditions ${GT} --eventcontent RAWSIM --datatier GEN-SIM-RAW --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --fileout ${gensimfile} --python_filename aNTGC_GJets_step1.py --beamspot ${beamspotConditions}  --pileup_input ${pileupDataset} --datamix PreMix --no_exec

sed -i 's#Configuration.StandardSequences.Services_cff'"'"')#Configuration.StandardSequences.Services_cff'"'"')\nimport random\nprocess.RandomNumberGeneratorService.generator.initialSeed = cms.untracked.uint32(random.randint(1000, 100000))#' aNTGC_GJets_step1.py


echo "Step 1 (GENSIM) python config file generation complete!"
current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo $current_date_time
printf "\n\n\n"

cmsDriver.py aNTGC_GJets.py  --nThreads ${nThreads2} --mc --step RAW2DIGI,L1Reco,RECO,RECOSIM,EI,PAT -n -1 --conditions ${GT} --eventcontent MINIAODSIM --runUnscheduled --datatier AODSIM-MINIAODSIM --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --filein file:${gensimfile} --fileout ${miniaodsimfile} --python_filename aNTGC_GJets_step2.py --no_exec


sed -i 's#Configuration.StandardSequences.Services_cff'"'"')#Configuration.StandardSequences.Services_cff'"'"')\nimport random\nprocess.RandomNumberGeneratorService.generator.initialSeed = cms.untracked.uint32(random.randint(1000, 100000))#' aNTGC_GJets_step2.py

echo "Step 2 (MINIAODSIM) python config file generation complete!"
current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo $current_date_time