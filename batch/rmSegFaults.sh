#!/bin/bash

searchDir=$1

find $searchDir -mindepth 1 -type d | while read line; do
	iDirName=$(basename $line)
	iDir=${searchDir}/${iDirName}/
	fileCheck=${searchDir}/${iDirName}/${iDirName}.log
	if [ -f ${fileCheck} ]; then
		if grep -q "Segmentation fault" "$fileCheck"; then
			rm -rf $iDir
			echo $iDir
		fi
	fi

	if [ ! -f ${fileCheck} ]; then
		# rm -rf $iDir
		echo $iDir
	fi

	# if [ -z $(ls -A ${fileCheck}) ]; then
	# 	rm -rf $iDir
	# 	echo $iDir
	# fi

	fileCheck=${searchDir}/${iDirName}/${iDirName}_MINIAODSIM.root
	if [ ! -f ${fileCheck} ]; then
		rm -rf $iDir
		echo $iDir
	fi

	# miniaodFile=${searchDir}/${iDirName}/${iDirName}_MINIAODSIM.log
done