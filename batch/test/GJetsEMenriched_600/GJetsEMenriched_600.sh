#!/bin/bash

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "Begin script @ " $current_date_time

echo "Running on: "

hostname

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/; eval `scramv1 runtime -sh`; cd -;

cd /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600/
current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "Begin GENSIM step @ " $current_date_time

cmsRun /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600_s1.py 

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "GENSIM step complete @ " $current_date_time

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "Begin MINIAOD step @ " $current_date_time

cmsRun /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600_s2.py

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "MINIAOD step complete @ " $current_date_time

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "End script @ " $current_date_time
