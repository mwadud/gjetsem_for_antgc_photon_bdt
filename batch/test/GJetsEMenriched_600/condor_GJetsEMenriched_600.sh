executable				= /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600.sh
output                	= /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600.log
error                 	= /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600.log
log                   	= /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600.log
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
Requirements 			= (Machine != "zebra01.spa.umn.edu")
transfer_input_files	= /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600_s1.py, /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600_s2.py
request_cpus 				= 4
request_memory			= 8000M
request_disk			= 5000M
+JobFlavour 			= "tomorrow"
queue
