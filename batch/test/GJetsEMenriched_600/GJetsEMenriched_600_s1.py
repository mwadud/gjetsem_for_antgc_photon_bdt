# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: aNTGC_GJets.py --nThreads 4 --mc --step GEN,SIM,DIGIPREMIX_S2,DATAMIX,L1,DIGI2RAW,HLT -n 10 --conditions 94X_mc2017_realistic_v17 --eventcontent RAWSIM --datatier GEN-SIM-RAW --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --fileout /local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600_GENSIM.root --python_filename aNTGC_GJets_step1.py --beamspot Realistic25ns13TeVEarly2017Collision --pileup_input puFile --datamix PreMix --no_exec
import FWCore.ParameterSet.Config as cms

from Configuration.StandardSequences.Eras import eras

process = cms.Process('HLT',eras.Run2_2017)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
import random
process.RandomNumberGeneratorService.generator.initialSeed = cms.untracked.uint32(random.randint(1000, 100000))
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.Geometry.GeometrySimDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.Generator_cff')
process.load('IOMC.EventVertexGenerators.VtxSmearedRealistic25ns13TeVEarly2017Collision_cfi')
process.load('GeneratorInterface.Core.genFilterSummary_cff')
process.load('Configuration.StandardSequences.SimIdeal_cff')
process.load('Configuration.StandardSequences.DigiDMPreMix_cff')
process.load('SimGeneral.MixingModule.digi_MixPreMix_cfi')
process.load('Configuration.StandardSequences.DataMixerPreMix_cff')
process.load('Configuration.StandardSequences.SimL1EmulatorDM_cff')
process.load('Configuration.StandardSequences.DigiToRawDM_cff')
process.load('HLTrigger.Configuration.HLT_GRun_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(10)
)

# Input source
process.source = cms.Source("EmptySource")

process.options = cms.untracked.PSet(

)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('aNTGC_GJets.py nevts:10'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition

process.RAWSIMoutput = cms.OutputModule("PoolOutputModule",
    SelectEvents = cms.untracked.PSet(
        SelectEvents = cms.vstring('generation_step')
    ),
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(9),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM-RAW'),
        filterName = cms.untracked.string('')
    ),
    eventAutoFlushCompressedSize = cms.untracked.int32(20971520),
    fileName = cms.untracked.string('/local/cms/user/wadud/aNTGCmet/slc6/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_600//GJetsEMenriched_600_GENSIM.root'),
    outputCommands = process.RAWSIMEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
process.genstepfilter.triggerConditions=cms.vstring("generation_step")
process.mix.digitizers = cms.PSet(process.theDigitizersMixPreMix)
process.mixData.input.fileNames = cms.untracked.vstring(['file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40019/6AF61EC5-B610-E811-895F-0CC47A78A3E8.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20027/8C548839-6B12-E811-863F-7845C4FC3758.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80003/0C875498-980C-E811-BE50-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20030/5C2CB90E-DC11-E811-9535-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20024/1A8C9501-4412-E811-8F27-002618FDA26D.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20008/685F4EE9-250F-E811-833E-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20007/C84184BD-6910-E811-90C8-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00019/9464839C-5D0E-E811-A95E-0CC47A78A456.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80002/367FC2AA-F40D-E811-A90A-0CC47A4C8E98.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40002/ACDA1731-760C-E811-AF70-008CFAFBEBF2.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20020/6E5A71DC-3010-E811-A015-0CC47A7C345E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00048/6233A6D0-6A12-E811-B87C-0CC47A4C8E86.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20025/DCE72553-FF10-E811-AF09-0025905B85E8.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00046/B62A311F-C312-E811-8C82-0CC47A4D762E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20031/8CA03600-0012-E811-B690-0CC47A4D7694.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80021/EE49224A-F40E-E811-BCA7-7CD30AD09C7C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80008/CA43B342-EF0E-E811-9D28-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20005/6C64B1C7-7F10-E811-9ADE-0025905A608C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20021/5AE04286-8410-E811-BE1E-0CC47A4D7634.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00022/D6566F8D-7710-E811-ABA1-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00018/AA654BDA-4310-E811-AF83-0CC47A4D768E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00024/4AFD2448-660F-E811-A56D-0CC47A78A436.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80007/7ACC77BD-410D-E811-B658-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20025/C62E5CA3-4B12-E811-BD34-0CC47A4D7692.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80008/6E715C6A-670D-E811-A45E-0CC47A4D763C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40022/1CB6C69A-0D11-E811-AE94-0CC47A74524E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80000/82496F9A-1F0D-E811-97B6-0CC47A4C8EB0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00037/3CEA774A-1112-E811-B4E3-0CC47A7C3424.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80029/F45E8BAE-9F11-E811-BC57-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20013/AA786D01-BC0E-E811-9A17-0CC47A4C8E2A.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20007/62DC1B70-600E-E811-8E17-0CC47A7C347A.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00019/1E4126A7-670E-E811-AD05-3417EBE65F65.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00026/E0EEF62C-A00F-E811-A097-0CC47A4C8F2C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00015/74CB7B09-E40D-E811-B7E2-0025905A6094.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00044/7EF9929E-E711-E811-83E9-0CC47A745282.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40045/4EE0F7C8-1D13-E811-8FE2-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00048/4A678928-F512-E811-838D-7CD30ACE18E8.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80003/5EC5D516-9F0C-E811-A726-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20022/F26ABF42-2212-E811-9F09-0CC47A4D768E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40028/C2D343EC-C20F-E811-BE25-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00015/608A4B1B-D50D-E811-80FE-0CC47A4D7668.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80033/3C87CA58-A110-E811-B799-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40013/708E16C3-2810-E811-8255-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40018/3A3AA381-B90E-E811-9B1D-0CC47A7C354A.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80000/7A7EEF31-080D-E811-A072-008CFAF74A32.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20008/42148D2D-2D0F-E811-B009-0CC47A4D76C0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40010/282D8AF2-4A0D-E811-9FCB-0025905B85B6.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40042/CE2DC7C5-FC12-E811-A882-0025905B85A0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20031/CE8F593C-FC11-E811-BB40-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40029/84DB00FC-EA0F-E811-8EBB-0242AC130002.root'])
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '94X_mc2017_realistic_v17', '')

process.gj_filter = cms.EDFilter("PythiaFilterGammaGamma",
    AcceptPrompts = cms.untracked.bool(True),
    EnergyCut = cms.untracked.double(1.0),
    EtaElThr = cms.untracked.double(2.8),
    EtaGammaThr = cms.untracked.double(2.8),
    EtaMaxCandidate = cms.untracked.double(3.0),
    EtaSeedThr = cms.untracked.double(2.8),
    EtaTkThr = cms.untracked.double(2.2),
    InvMassMax = cms.untracked.double(13000.0),
    InvMassMin = cms.untracked.double(40.0),
    NTkConeMax = cms.untracked.int32(2),
    NTkConeSum = cms.untracked.int32(4),
    PromptPtThreshold = cms.untracked.double(15.0),
    PtElThr = cms.untracked.double(2.0),
    PtGammaThr = cms.untracked.double(0.0),
    PtMinCandidate1 = cms.untracked.double(15.0),
    PtMinCandidate2 = cms.untracked.double(15.0),
    PtSeedThr = cms.untracked.double(5.0),
    PtTkThr = cms.untracked.double(1.6),
    dEtaSeedMax = cms.untracked.double(0.12),
    dPhiSeedMax = cms.untracked.double(0.2),
    dRNarrowCone = cms.untracked.double(0.02),
    dRSeedMax = cms.untracked.double(0.0),
    dRTkMax = cms.untracked.double(0.2)
)


process.pho_filter = cms.EDFilter("PythiaFilter",
    MaxEta = cms.untracked.double(2.52),
    MaxPt = cms.untracked.double(1200.0),
    MinEta = cms.untracked.double(-2.52),
    MinPt = cms.untracked.double(180.0),
    ParticleID = cms.untracked.int32(22),
    Status = cms.untracked.int32(23)
)


process.generator = cms.EDFilter("Pythia8GeneratorFilter",
    PythiaParameters = cms.PSet(
        parameterSets = cms.vstring('pythia8CommonSettings', 
            'pythia8CP5Settings', 
            'pythia8PSweightsSettings', 
            'processParameters'),
        processParameters = cms.vstring('PromptPhoton:qg2qgamma = on       ! prompt photon production', 
            'PromptPhoton:qqbar2ggamma = on    ! prompt photon production', 
            'PromptPhoton:gg2ggamma = on       ! prompt photon production', 
            'PhaseSpace:pTHatMin = 20.      \t! minimum pt hat for hard interactions', 
            'PhaseSpace:pTHatMax = -1          ! maximum pt hat for hard interactions'),
        pythia8CP5Settings = cms.vstring('Tune:pp 14', 
            'Tune:ee 7', 
            'MultipartonInteractions:ecmPow=0.03344', 
            'PDF:pSet=20', 
            'MultipartonInteractions:bProfile=2', 
            'MultipartonInteractions:pT0Ref=1.41', 
            'MultipartonInteractions:coreRadius=0.7634', 
            'MultipartonInteractions:coreFraction=0.63', 
            'ColourReconnection:range=5.176', 
            'SigmaTotal:zeroAXB=off', 
            'SpaceShower:alphaSorder=2', 
            'SpaceShower:alphaSvalue=0.118', 
            'SigmaProcess:alphaSvalue=0.118', 
            'SigmaProcess:alphaSorder=2', 
            'MultipartonInteractions:alphaSvalue=0.118', 
            'MultipartonInteractions:alphaSorder=2', 
            'TimeShower:alphaSorder=2', 
            'TimeShower:alphaSvalue=0.118'),
        pythia8CommonSettings = cms.vstring('Tune:preferLHAPDF = 2', 
            'Main:timesAllowErrors = 10000', 
            'Check:epTolErr = 0.01', 
            'Beams:setProductionScalesFromLHEF = off', 
            'SLHA:keepSM = on', 
            'SLHA:minMassSM = 1000.', 
            'ParticleDecays:limitTau0 = on', 
            'ParticleDecays:tau0Max = 10', 
            'ParticleDecays:allowPhotonRadiation = on'),
        pythia8PSweightsSettings = cms.vstring('UncertaintyBands:doVariations = on', 
            'UncertaintyBands:List = {isrRedHi isr:muRfac=0.707,fsrRedHi fsr:muRfac=0.707,isrRedLo isr:muRfac=1.414,fsrRedLo fsr:muRfac=1.414,isrDefHi isr:muRfac=0.5,fsrDefHi fsr:muRfac=0.5,isrDefLo isr:muRfac=2.0,fsrDefLo fsr:muRfac=2.0,isrConHi isr:muRfac=0.25,fsrConHi fsr:muRfac=0.25,isrConLo isr:muRfac=4.0,fsrConLo fsr:muRfac=4.0,fsr_G2GG_muR_dn fsr:G2GG:muRfac=0.5,fsr_G2GG_muR_up fsr:G2GG:muRfac=2.0,fsr_G2QQ_muR_dn fsr:G2QQ:muRfac=0.5,fsr_G2QQ_muR_up fsr:G2QQ:muRfac=2.0,fsr_Q2QG_muR_dn fsr:Q2QG:muRfac=0.5,fsr_Q2QG_muR_up fsr:Q2QG:muRfac=2.0,fsr_X2XG_muR_dn fsr:X2XG:muRfac=0.5,fsr_X2XG_muR_up fsr:X2XG:muRfac=2.0,fsr_G2GG_cNS_dn fsr:G2GG:cNS=-2.0,fsr_G2GG_cNS_up fsr:G2GG:cNS=2.0,fsr_G2QQ_cNS_dn fsr:G2QQ:cNS=-2.0,fsr_G2QQ_cNS_up fsr:G2QQ:cNS=2.0,fsr_Q2QG_cNS_dn fsr:Q2QG:cNS=-2.0,fsr_Q2QG_cNS_up fsr:Q2QG:cNS=2.0,fsr_X2XG_cNS_dn fsr:X2XG:cNS=-2.0,fsr_X2XG_cNS_up fsr:X2XG:cNS=2.0,isr_G2GG_muR_dn isr:G2GG:muRfac=0.5,isr_G2GG_muR_up isr:G2GG:muRfac=2.0,isr_G2QQ_muR_dn isr:G2QQ:muRfac=0.5,isr_G2QQ_muR_up isr:G2QQ:muRfac=2.0,isr_Q2QG_muR_dn isr:Q2QG:muRfac=0.5,isr_Q2QG_muR_up isr:Q2QG:muRfac=2.0,isr_X2XG_muR_dn isr:X2XG:muRfac=0.5,isr_X2XG_muR_up isr:X2XG:muRfac=2.0,isr_G2GG_cNS_dn isr:G2GG:cNS=-2.0,isr_G2GG_cNS_up isr:G2GG:cNS=2.0,isr_G2QQ_cNS_dn isr:G2QQ:cNS=-2.0,isr_G2QQ_cNS_up isr:G2QQ:cNS=2.0,isr_Q2QG_cNS_dn isr:Q2QG:cNS=-2.0,isr_Q2QG_cNS_up isr:Q2QG:cNS=2.0,isr_X2XG_cNS_dn isr:X2XG:cNS=-2.0,isr_X2XG_cNS_up isr:X2XG:cNS=2.0}', 
            'UncertaintyBands:nFlavQ = 4', 
            'UncertaintyBands:MPIshowers = on', 
            'UncertaintyBands:overSampleFSR = 10.0', 
            'UncertaintyBands:overSampleISR = 10.0', 
            'UncertaintyBands:FSRpTmin2Fac = 20', 
            'UncertaintyBands:ISRpTmin2Fac = 1')
    ),
    comEnergy = cms.double(13000.0),
    crossSection = cms.untracked.double(1.0),
    filterEfficiency = cms.untracked.double(1.0),
    maxEventsToPrint = cms.untracked.int32(0),
    pythiaHepMCVerbosity = cms.untracked.bool(True),
    pythiaPylistVerbosity = cms.untracked.int32(True),
    reweightGenEmp = cms.PSet(
        tune = cms.string('CP5')
    )
)


process.ProductionFilterSequence = cms.Sequence(process.generator+process.pho_filter+process.gj_filter)

# Path and EndPath definitions
process.generation_step = cms.Path(process.pgen)
process.simulation_step = cms.Path(process.psim)
process.digitisation_step = cms.Path(process.pdigi)
process.datamixing_step = cms.Path(process.pdatamix)
process.L1simulation_step = cms.Path(process.SimL1Emulator)
process.digi2raw_step = cms.Path(process.DigiToRaw)
process.genfiltersummary_step = cms.EndPath(process.genFilterSummary)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RAWSIMoutput_step = cms.EndPath(process.RAWSIMoutput)

# Schedule definition
process.schedule = cms.Schedule(process.generation_step,process.genfiltersummary_step,process.simulation_step,process.digitisation_step,process.datamixing_step,process.L1simulation_step,process.digi2raw_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.extend([process.endjob_step,process.RAWSIMoutput_step])
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

#Setup FWK for multithreaded
process.options.numberOfThreads=cms.untracked.uint32(4)
process.options.numberOfStreams=cms.untracked.uint32(0)
# filter all path with the production filter sequence
for path in process.paths:
	getattr(process,path)._seq = process.ProductionFilterSequence * getattr(process,path)._seq 

# customisation of the process.

# Automatic addition of the customisation function from Configuration.DataProcessing.Utils
from Configuration.DataProcessing.Utils import addMonitoring 

#call to customisation function addMonitoring imported from Configuration.DataProcessing.Utils
process = addMonitoring(process)

# Automatic addition of the customisation function from HLTrigger.Configuration.customizeHLTforMC
from HLTrigger.Configuration.customizeHLTforMC import customizeHLTforMC 

#call to customisation function customizeHLTforMC imported from HLTrigger.Configuration.customizeHLTforMC
process = customizeHLTforMC(process)

# End of customisation functions

# Customisation from command line

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
