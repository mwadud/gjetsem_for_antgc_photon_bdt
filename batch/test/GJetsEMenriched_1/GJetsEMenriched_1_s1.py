# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: aNTGC_GJets.py --nThreads 4 --mc --step GEN,SIM,DIGIPREMIX_S2,DATAMIX,L1,DIGI2RAW,HLT -n 200 --conditions 94X_mc2017_realistic_v17 --eventcontent RAWSIM --datatier GEN-SIM-RAW --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --fileout /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1_GENSIM.root --python_filename aNTGC_GJets_step1.py --beamspot Realistic25ns13TeVEarly2017Collision --pileup_input puFile --datamix PreMix --no_exec
import FWCore.ParameterSet.Config as cms

from Configuration.StandardSequences.Eras import eras

process = cms.Process('HLT',eras.Run2_2017)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
import random
process.RandomNumberGeneratorService.generator.initialSeed = cms.untracked.uint32(random.randint(1000, 100000))
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.Geometry.GeometrySimDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.Generator_cff')
process.load('IOMC.EventVertexGenerators.VtxSmearedRealistic25ns13TeVEarly2017Collision_cfi')
process.load('GeneratorInterface.Core.genFilterSummary_cff')
process.load('Configuration.StandardSequences.SimIdeal_cff')
process.load('Configuration.StandardSequences.DigiDMPreMix_cff')
process.load('SimGeneral.MixingModule.digi_MixPreMix_cfi')
process.load('Configuration.StandardSequences.DataMixerPreMix_cff')
process.load('Configuration.StandardSequences.SimL1EmulatorDM_cff')
process.load('Configuration.StandardSequences.DigiToRawDM_cff')
process.load('HLTrigger.Configuration.HLT_GRun_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(10)
)

# Input source
process.source = cms.Source("EmptySource")

process.options = cms.untracked.PSet(

)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('aNTGC_GJets.py nevts:200'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition

process.RAWSIMoutput = cms.OutputModule("PoolOutputModule",
    SelectEvents = cms.untracked.PSet(
        SelectEvents = cms.vstring('generation_step')
    ),
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(9),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM-RAW'),
        filterName = cms.untracked.string('')
    ),
    eventAutoFlushCompressedSize = cms.untracked.int32(20971520),
    fileName = cms.untracked.string('/local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1_GENSIM.root'),
    outputCommands = process.RAWSIMEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
process.genstepfilter.triggerConditions=cms.vstring("generation_step")
process.mix.digitizers = cms.PSet(process.theDigitizersMixPreMix)
process.mixData.input.fileNames = cms.untracked.vstring(['file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80040/14C24932-DE12-E811-9527-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80000/987A9575-7D0C-E811-BFF7-0CC47A7C3420.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00009/903F61EB-550D-E811-9574-0025905A608E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40028/7EE96949-A311-E811-9AB0-0CC47A7C35A4.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40004/FE317B01-AD0C-E811-B577-0CC47A78A4B0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80032/EA0A6D42-7010-E811-AA49-7845C4FC35BD.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00028/1491D1C0-EC0F-E811-A610-0CC47A7C357E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40040/C0DE2B2D-CA12-E811-A8D1-0CC47A4C8ED8.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00015/608A4B1B-D50D-E811-80FE-0CC47A4D7668.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40002/90EAA6F4-AB0D-E811-9BCC-0CC47A78A440.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20008/42148D2D-2D0F-E811-B009-0CC47A4D76C0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20019/02EBFF05-0E10-E811-9141-0025905B85BC.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40008/D8D115D5-320D-E811-8B5A-0CC47A7C35F8.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40022/1CB6C69A-0D11-E811-AE94-0CC47A74524E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20012/00976677-0F0F-E811-9C72-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20004/749B5939-920E-E811-B07A-0025905A48D0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20007/62DC1B70-600E-E811-8E17-0CC47A7C347A.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20031/CE8F593C-FC11-E811-BB40-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80035/B6265D93-6D12-E811-B735-0CC47A4D762E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20003/C6F36C5C-9D0E-E811-B9D7-0025905B856C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00044/7EF9929E-E711-E811-83E9-0CC47A745282.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40010/282D8AF2-4A0D-E811-9FCB-0025905B85B6.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40013/D4B98B81-210E-E811-9D7D-0025905A48D6.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20007/E448F080-5F0E-E811-85FC-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80036/F6FBE787-7E12-E811-A96A-0CC47A4D7634.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00046/B62A311F-C312-E811-8C82-0CC47A4D762E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40034/C436B850-FA12-E811-83A7-0CC47A4C8E56.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00048/4A678928-F512-E811-838D-7CD30ACE18E8.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20034/6E6E5661-6F12-E811-B57D-0CC47A7C34C4.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40014/72976199-E00D-E811-B294-0CC47A7452D0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40026/0C0511E7-A20F-E811-B775-0CC47A4C8E5E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40028/C2D343EC-C20F-E811-BE25-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00015/FC970CF0-DB0D-E811-84EF-0CC47A7C34E6.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40017/F484A969-9210-E811-894A-0CC47A4C8F0C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80003/5EC5D516-9F0C-E811-A726-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00036/82A01B88-D411-E811-8066-0CC47A4C8E1E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20032/5EC96E08-E212-E811-9E52-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20025/C62E5CA3-4B12-E811-BD34-0CC47A4D7692.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00042/FADCB917-7E12-E811-AFA0-7CD30AD09B02.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20016/7ADD95E4-7F0F-E811-B726-0CC47A4C8E82.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20034/6453A4A7-5314-E811-AF31-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80042/E2428B1F-0113-E811-80B9-0CC47A4C8F18.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80009/22480378-1C0F-E811-8458-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40028/54715ABD-AB0F-E811-9322-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40029/84DB00FC-EA0F-E811-8EBB-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00022/7602A8CB-100F-E811-A107-0025905B85EC.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40032/94A6B3AD-5510-E811-9145-7845C4FC3B87.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80039/1050D08F-C512-E811-AE2C-008CFAF73286.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80021/EE49224A-F40E-E811-BCA7-7CD30AD09C7C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80000/82496F9A-1F0D-E811-97B6-0CC47A4C8EB0.root'])
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '94X_mc2017_realistic_v17', '')

process.gj_filter = cms.EDFilter("PythiaFilterGammaGamma",
    AcceptPrompts = cms.untracked.bool(True),
    EnergyCut = cms.untracked.double(1.0),
    EtaElThr = cms.untracked.double(2.8),
    EtaGammaThr = cms.untracked.double(1.48),
    EtaMaxCandidate = cms.untracked.double(3.0),
    EtaSeedThr = cms.untracked.double(2.8),
    EtaTkThr = cms.untracked.double(2.2),
    InvMassMax = cms.untracked.double(13000.0),
    InvMassMin = cms.untracked.double(0.0),
    NTkConeMax = cms.untracked.int32(50),
    NTkConeSum = cms.untracked.int32(100),
    PromptPtThreshold = cms.untracked.double(15.0),
    PtElThr = cms.untracked.double(2.0),
    PtGammaThr = cms.untracked.double(180.0),
    PtMinCandidate1 = cms.untracked.double(180.0),
    PtMinCandidate2 = cms.untracked.double(15.0),
    PtSeedThr = cms.untracked.double(5.0),
    PtTkThr = cms.untracked.double(1.6),
    dEtaSeedMax = cms.untracked.double(0.12),
    dPhiSeedMax = cms.untracked.double(0.2),
    dRNarrowCone = cms.untracked.double(0.02),
    dRSeedMax = cms.untracked.double(0.0),
    dRTkMax = cms.untracked.double(0.2)
)


process.pho_filter = cms.EDFilter("PythiaFilter",
    MaxEta = cms.untracked.double(1.48),
    MaxPt = cms.untracked.double(1000.0),
    MinEta = cms.untracked.double(-1.48),
    MinPt = cms.untracked.double(180.0),
    ParticleID = cms.untracked.int32(22),
    Status = cms.untracked.int32(23)
)


process.generator = cms.EDFilter("Pythia8GeneratorFilter",
    PythiaParameters = cms.PSet(
        parameterSets = cms.vstring('pythia8CommonSettings', 
            'pythia8CP5Settings', 
            'pythia8PSweightsSettings', 
            'processParameters'),
        processParameters = cms.vstring('PromptPhoton:qg2qgamma = on       ! prompt photon production', 
            'PromptPhoton:qqbar2ggamma = on    ! prompt photon production', 
            'PromptPhoton:gg2ggamma = on       ! prompt photon production', 
            'PhaseSpace:pTHatMin = 40.      \t! minimum pt hat for hard interactions', 
            'PhaseSpace:pTHatMax = -1          ! maximum pt hat for hard interactions'),
        pythia8CP5Settings = cms.vstring('Tune:pp 14', 
            'Tune:ee 7', 
            'MultipartonInteractions:ecmPow=0.03344', 
            'PDF:pSet=20', 
            'MultipartonInteractions:bProfile=2', 
            'MultipartonInteractions:pT0Ref=1.41', 
            'MultipartonInteractions:coreRadius=0.7634', 
            'MultipartonInteractions:coreFraction=0.63', 
            'ColourReconnection:range=5.176', 
            'SigmaTotal:zeroAXB=off', 
            'SpaceShower:alphaSorder=2', 
            'SpaceShower:alphaSvalue=0.118', 
            'SigmaProcess:alphaSvalue=0.118', 
            'SigmaProcess:alphaSorder=2', 
            'MultipartonInteractions:alphaSvalue=0.118', 
            'MultipartonInteractions:alphaSorder=2', 
            'TimeShower:alphaSorder=2', 
            'TimeShower:alphaSvalue=0.118'),
        pythia8CommonSettings = cms.vstring('Tune:preferLHAPDF = 2', 
            'Main:timesAllowErrors = 10000', 
            'Check:epTolErr = 0.01', 
            'Beams:setProductionScalesFromLHEF = off', 
            'SLHA:keepSM = on', 
            'SLHA:minMassSM = 1000.', 
            'ParticleDecays:limitTau0 = on', 
            'ParticleDecays:tau0Max = 10', 
            'ParticleDecays:allowPhotonRadiation = on'),
        pythia8PSweightsSettings = cms.vstring('UncertaintyBands:doVariations = on', 
            'UncertaintyBands:List = {isrRedHi isr:muRfac=0.707,fsrRedHi fsr:muRfac=0.707,isrRedLo isr:muRfac=1.414,fsrRedLo fsr:muRfac=1.414,isrDefHi isr:muRfac=0.5,fsrDefHi fsr:muRfac=0.5,isrDefLo isr:muRfac=2.0,fsrDefLo fsr:muRfac=2.0,isrConHi isr:muRfac=0.25,fsrConHi fsr:muRfac=0.25,isrConLo isr:muRfac=4.0,fsrConLo fsr:muRfac=4.0,fsr_G2GG_muR_dn fsr:G2GG:muRfac=0.5,fsr_G2GG_muR_up fsr:G2GG:muRfac=2.0,fsr_G2QQ_muR_dn fsr:G2QQ:muRfac=0.5,fsr_G2QQ_muR_up fsr:G2QQ:muRfac=2.0,fsr_Q2QG_muR_dn fsr:Q2QG:muRfac=0.5,fsr_Q2QG_muR_up fsr:Q2QG:muRfac=2.0,fsr_X2XG_muR_dn fsr:X2XG:muRfac=0.5,fsr_X2XG_muR_up fsr:X2XG:muRfac=2.0,fsr_G2GG_cNS_dn fsr:G2GG:cNS=-2.0,fsr_G2GG_cNS_up fsr:G2GG:cNS=2.0,fsr_G2QQ_cNS_dn fsr:G2QQ:cNS=-2.0,fsr_G2QQ_cNS_up fsr:G2QQ:cNS=2.0,fsr_Q2QG_cNS_dn fsr:Q2QG:cNS=-2.0,fsr_Q2QG_cNS_up fsr:Q2QG:cNS=2.0,fsr_X2XG_cNS_dn fsr:X2XG:cNS=-2.0,fsr_X2XG_cNS_up fsr:X2XG:cNS=2.0,isr_G2GG_muR_dn isr:G2GG:muRfac=0.5,isr_G2GG_muR_up isr:G2GG:muRfac=2.0,isr_G2QQ_muR_dn isr:G2QQ:muRfac=0.5,isr_G2QQ_muR_up isr:G2QQ:muRfac=2.0,isr_Q2QG_muR_dn isr:Q2QG:muRfac=0.5,isr_Q2QG_muR_up isr:Q2QG:muRfac=2.0,isr_X2XG_muR_dn isr:X2XG:muRfac=0.5,isr_X2XG_muR_up isr:X2XG:muRfac=2.0,isr_G2GG_cNS_dn isr:G2GG:cNS=-2.0,isr_G2GG_cNS_up isr:G2GG:cNS=2.0,isr_G2QQ_cNS_dn isr:G2QQ:cNS=-2.0,isr_G2QQ_cNS_up isr:G2QQ:cNS=2.0,isr_Q2QG_cNS_dn isr:Q2QG:cNS=-2.0,isr_Q2QG_cNS_up isr:Q2QG:cNS=2.0,isr_X2XG_cNS_dn isr:X2XG:cNS=-2.0,isr_X2XG_cNS_up isr:X2XG:cNS=2.0}', 
            'UncertaintyBands:nFlavQ = 4', 
            'UncertaintyBands:MPIshowers = on', 
            'UncertaintyBands:overSampleFSR = 10.0', 
            'UncertaintyBands:overSampleISR = 10.0', 
            'UncertaintyBands:FSRpTmin2Fac = 20', 
            'UncertaintyBands:ISRpTmin2Fac = 1')
    ),
    comEnergy = cms.double(13000.0),
    crossSection = cms.untracked.double(1.0),
    filterEfficiency = cms.untracked.double(1.0),
    maxEventsToPrint = cms.untracked.int32(0),
    pythiaHepMCVerbosity = cms.untracked.bool(True),
    pythiaPylistVerbosity = cms.untracked.int32(True),
    reweightGenEmp = cms.PSet(
        tune = cms.string('CP5')
    )
)


process.ProductionFilterSequence = cms.Sequence(process.generator+process.pho_filter+process.gj_filter)

# Path and EndPath definitions
process.generation_step = cms.Path(process.pgen)
process.simulation_step = cms.Path(process.psim)
process.digitisation_step = cms.Path(process.pdigi)
process.datamixing_step = cms.Path(process.pdatamix)
process.L1simulation_step = cms.Path(process.SimL1Emulator)
process.digi2raw_step = cms.Path(process.DigiToRaw)
process.genfiltersummary_step = cms.EndPath(process.genFilterSummary)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RAWSIMoutput_step = cms.EndPath(process.RAWSIMoutput)

# Schedule definition
process.schedule = cms.Schedule(process.generation_step,process.genfiltersummary_step,process.simulation_step,process.digitisation_step,process.datamixing_step,process.L1simulation_step,process.digi2raw_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.extend([process.endjob_step,process.RAWSIMoutput_step])
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

#Setup FWK for multithreaded
process.options.numberOfThreads=cms.untracked.uint32(4)
process.options.numberOfStreams=cms.untracked.uint32(0)
# filter all path with the production filter sequence
for path in process.paths:
	getattr(process,path)._seq = process.ProductionFilterSequence * getattr(process,path)._seq 

# customisation of the process.

# Automatic addition of the customisation function from Configuration.DataProcessing.Utils
from Configuration.DataProcessing.Utils import addMonitoring 

#call to customisation function addMonitoring imported from Configuration.DataProcessing.Utils
process = addMonitoring(process)

# Automatic addition of the customisation function from HLTrigger.Configuration.customizeHLTforMC
from HLTrigger.Configuration.customizeHLTforMC import customizeHLTforMC 

#call to customisation function customizeHLTforMC imported from HLTrigger.Configuration.customizeHLTforMC
process = customizeHLTforMC(process)

# End of customisation functions

# Customisation from command line

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
