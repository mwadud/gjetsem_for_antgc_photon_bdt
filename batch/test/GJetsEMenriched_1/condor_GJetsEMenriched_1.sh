executable				= /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1.sh
output                	= /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1.log
error                 	= /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1.log
log                   	= /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1.log
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
#transfer_output_files	= /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1.root
transfer_input_files	= /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1_s1.py, /local/cms/user/wadud/aNTGCmet/CMSSW_9_4_17/src/Configuration/Generator/generateGJets4ID/batch/test//GJetsEMenriched_1//GJetsEMenriched_1_s2.py
request_cpus 				= 4
request_memory			= 8000M
request_disk			= 10000M
+JobFlavour 			= "longlunch"
queue
