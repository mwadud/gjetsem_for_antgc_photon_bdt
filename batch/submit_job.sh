#!/bin/bash

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "Begin script @ " $current_date_time

echo "Running on: "

hostname

source #cmssetsh
cd #cmsswdir; eval `scramv1 runtime -sh`; cd -;

cd #jobdir
current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "Begin GENSIM step @ " $current_date_time

cmsRun #step1cfg 

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "GENSIM step complete @ " $current_date_time

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "Begin MINIAOD step @ " $current_date_time

cmsRun #step2cfg

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "MINIAOD step complete @ " $current_date_time

current_date_time="`date +%Y-%m-%d-%H:%M:%S`";
echo "End script @ " $current_date_time
