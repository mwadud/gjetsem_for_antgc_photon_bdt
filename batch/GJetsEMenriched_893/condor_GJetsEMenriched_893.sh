executable				= /local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893.sh
output                	= /local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893.log
error                 	= /local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893.log
log                   	= /local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893.log
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
Requirements 			= (Machine != "zebra*.spa.umn.edu")
transfer_input_files	= /local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893_s1.py, /local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893_s2.py
request_cpus 				= 4
request_memory			= 8000M
request_disk			= 5000M
+JobFlavour 			= "testmatch"
queue
