# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: aNTGC_GJets.py --nThreads 4 --mc --step GEN,SIM,DIGIPREMIX_S2,DATAMIX,L1,DIGI2RAW,HLT -n 5000 --conditions 94X_mc2017_realistic_v17 --eventcontent RAWSIM --datatier GEN-SIM-RAW --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --fileout /local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893_GENSIM.root --python_filename aNTGC_GJets_step1.py --beamspot Realistic25ns13TeVEarly2017Collision --pileup_input puFile --datamix PreMix --no_exec
import FWCore.ParameterSet.Config as cms

from Configuration.StandardSequences.Eras import eras

process = cms.Process('HLT',eras.Run2_2017)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
import random
process.RandomNumberGeneratorService.generator.initialSeed = cms.untracked.uint32(random.randint(1000, 100000))
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.Geometry.GeometrySimDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.Generator_cff')
process.load('IOMC.EventVertexGenerators.VtxSmearedRealistic25ns13TeVEarly2017Collision_cfi')
process.load('GeneratorInterface.Core.genFilterSummary_cff')
process.load('Configuration.StandardSequences.SimIdeal_cff')
process.load('Configuration.StandardSequences.DigiDMPreMix_cff')
process.load('SimGeneral.MixingModule.digi_MixPreMix_cfi')
process.load('Configuration.StandardSequences.DataMixerPreMix_cff')
process.load('Configuration.StandardSequences.SimL1EmulatorDM_cff')
process.load('Configuration.StandardSequences.DigiToRawDM_cff')
process.load('HLTrigger.Configuration.HLT_GRun_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(5000)
)

# Input source
process.source = cms.Source("EmptySource")

process.options = cms.untracked.PSet(

)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('aNTGC_GJets.py nevts:5000'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition

process.RAWSIMoutput = cms.OutputModule("PoolOutputModule",
    SelectEvents = cms.untracked.PSet(
        SelectEvents = cms.vstring('generation_step')
    ),
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(9),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM-RAW'),
        filterName = cms.untracked.string('')
    ),
    eventAutoFlushCompressedSize = cms.untracked.int32(20971520),
    fileName = cms.untracked.string('/local/cms/user/wadud/aNTGCmet/simulations/GJetsEMenriched/Prod_2020_04_15//GJetsEMenriched_893//GJetsEMenriched_893_GENSIM.root'),
    outputCommands = process.RAWSIMEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
process.genstepfilter.triggerConditions=cms.vstring("generation_step")
process.mix.digitizers = cms.PSet(process.theDigitizersMixPreMix)
process.mixData.input.fileNames = cms.untracked.vstring(['file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20000/A2FAD49B-650C-E811-94F9-7CD30AD0A1EC.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40022/ACFD70D4-260F-E811-9ABB-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80003/0C875498-980C-E811-BE50-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80038/8C0A91E3-A712-E811-A81F-E0DB55FDA545.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40028/7EE96949-A311-E811-9AB0-0CC47A7C35A4.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20009/1EA2027E-FD0E-E811-9AF1-0CC47A4D76C0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00018/AA654BDA-4310-E811-AF83-0CC47A4D768E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40018/96FAD950-C90E-E811-80B6-003048FFD72C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20003/AC0D5EA7-A20E-E811-BFC5-0CC47A4D762A.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20034/6453A4A7-5314-E811-AF31-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20012/B82C0B16-1F0F-E811-876C-7CD30AD089E0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00018/443C7996-810E-E811-9D55-0CC47A78A360.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20024/1A8C9501-4412-E811-8F27-002618FDA26D.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40036/B2589B02-8D12-E811-BBBB-0CC47A4D7654.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20027/8C548839-6B12-E811-863F-7845C4FC3758.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20011/1C610FBC-400F-E811-B622-0025905B860C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20008/685F4EE9-250F-E811-833E-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20014/AA32DD0F-6D11-E811-BBA0-0CC47A4D7654.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40000/36E39466-630C-E811-8527-7CD30AD091C6.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20034/6E6E5661-6F12-E811-B57D-0CC47A7C34C4.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40009/E087B63A-710F-E811-B65E-0CC47A4C8F12.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80008/6E715C6A-670D-E811-A45E-0CC47A4D763C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20014/8E19167B-640F-E811-9CF5-0CC47A7C3432.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40015/90C16C76-3C0E-E811-865E-0025905A497A.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20025/C62E5CA3-4B12-E811-BD34-0CC47A4D7692.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20027/60DE9C80-6B12-E811-A2EC-0CC47A7C35D2.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40012/0EDA7D82-EB0F-E811-BFDD-0025905B855A.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00047/C489D57A-E212-E811-9D2E-0CC47A4D766C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20004/749B5939-920E-E811-B07A-0025905A48D0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80002/367FC2AA-F40D-E811-A90A-0CC47A4C8E98.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80033/3C87CA58-A110-E811-B799-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00026/92D24EED-A60F-E811-8F31-008CFAFC53C6.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80035/B6265D93-6D12-E811-B735-0CC47A4D762E.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80021/EE49224A-F40E-E811-BCA7-7CD30AD09C7C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40019/62D3268A-D00E-E811-8610-0CC47A4D7638.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20016/16FB5217-9A0F-E811-95C8-0025905A610C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20033/B2F294A6-3A14-E811-BB8B-0025905B8576.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40004/FE317B01-AD0C-E811-B577-0CC47A78A4B0.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/80007/D013754C-400D-E811-B04A-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20012/00976677-0F0F-E811-9C72-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00027/BE9A3EEB-740F-E811-B9C3-0CC47A7C3432.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00025/F2204719-9C10-E811-BE90-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00019/60A10037-2710-E811-812C-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20023/660F0449-6610-E811-9F1F-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40008/AC23E424-2B0D-E811-89AC-0CC47A78A478.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/00014/8E215FE8-870F-E811-ACDF-0CC47A4C8E96.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20027/AA1AE08A-5711-E811-93BA-0025905A48BA.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20022/B20A4278-2312-E811-933F-7845C4FC3B0C.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/40038/96371009-3711-E811-9507-0242AC130002.root', 'file:/local/cms/user/wadud/aNTGCmet/datasets/RunIISummer17PrePremix/Neutrino_E-10_gun/GEN-SIM-DIGI-RAW/MCv2_correctPU_94X_mc2017_realistic_v9-v1/20003/C6F36C5C-9D0E-E811-B9D7-0025905B856C.root'])
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '94X_mc2017_realistic_v17', '')

process.gj_filter = cms.EDFilter("PythiaFilterGammaGamma",
    AcceptPrompts = cms.untracked.bool(True),
    EnergyCut = cms.untracked.double(1.0),
    EtaElThr = cms.untracked.double(2.8),
    EtaGammaThr = cms.untracked.double(2.8),
    EtaMaxCandidate = cms.untracked.double(3.0),
    EtaSeedThr = cms.untracked.double(2.8),
    EtaTkThr = cms.untracked.double(2.2),
    InvMassMax = cms.untracked.double(13000.0),
    InvMassMin = cms.untracked.double(40.0),
    NTkConeMax = cms.untracked.int32(2),
    NTkConeSum = cms.untracked.int32(4),
    PromptPtThreshold = cms.untracked.double(15.0),
    PtElThr = cms.untracked.double(2.0),
    PtGammaThr = cms.untracked.double(0.0),
    PtMinCandidate1 = cms.untracked.double(15.0),
    PtMinCandidate2 = cms.untracked.double(15.0),
    PtSeedThr = cms.untracked.double(5.0),
    PtTkThr = cms.untracked.double(1.6),
    dEtaSeedMax = cms.untracked.double(0.12),
    dPhiSeedMax = cms.untracked.double(0.2),
    dRNarrowCone = cms.untracked.double(0.02),
    dRSeedMax = cms.untracked.double(0.0),
    dRTkMax = cms.untracked.double(0.2)
)


process.pho_filter = cms.EDFilter("PythiaFilter",
    MaxEta = cms.untracked.double(2.52),
    MaxPt = cms.untracked.double(1200.0),
    MinEta = cms.untracked.double(-2.52),
    MinPt = cms.untracked.double(180.0),
    ParticleID = cms.untracked.int32(22),
    Status = cms.untracked.int32(23)
)


process.generator = cms.EDFilter("Pythia8GeneratorFilter",
    PythiaParameters = cms.PSet(
        parameterSets = cms.vstring('pythia8CommonSettings', 
            'pythia8CP5Settings', 
            'pythia8PSweightsSettings', 
            'processParameters'),
        processParameters = cms.vstring('PromptPhoton:qg2qgamma = on       ! prompt photon production', 
            'PromptPhoton:qqbar2ggamma = on    ! prompt photon production', 
            'PromptPhoton:gg2ggamma = on       ! prompt photon production', 
            'PhaseSpace:pTHatMin = 20.      \t! minimum pt hat for hard interactions', 
            'PhaseSpace:pTHatMax = -1          ! maximum pt hat for hard interactions'),
        pythia8CP5Settings = cms.vstring('Tune:pp 14', 
            'Tune:ee 7', 
            'MultipartonInteractions:ecmPow=0.03344', 
            'PDF:pSet=20', 
            'MultipartonInteractions:bProfile=2', 
            'MultipartonInteractions:pT0Ref=1.41', 
            'MultipartonInteractions:coreRadius=0.7634', 
            'MultipartonInteractions:coreFraction=0.63', 
            'ColourReconnection:range=5.176', 
            'SigmaTotal:zeroAXB=off', 
            'SpaceShower:alphaSorder=2', 
            'SpaceShower:alphaSvalue=0.118', 
            'SigmaProcess:alphaSvalue=0.118', 
            'SigmaProcess:alphaSorder=2', 
            'MultipartonInteractions:alphaSvalue=0.118', 
            'MultipartonInteractions:alphaSorder=2', 
            'TimeShower:alphaSorder=2', 
            'TimeShower:alphaSvalue=0.118'),
        pythia8CommonSettings = cms.vstring('Tune:preferLHAPDF = 2', 
            'Main:timesAllowErrors = 10000', 
            'Check:epTolErr = 0.01', 
            'Beams:setProductionScalesFromLHEF = off', 
            'SLHA:keepSM = on', 
            'SLHA:minMassSM = 1000.', 
            'ParticleDecays:limitTau0 = on', 
            'ParticleDecays:tau0Max = 10', 
            'ParticleDecays:allowPhotonRadiation = on'),
        pythia8PSweightsSettings = cms.vstring('UncertaintyBands:doVariations = on', 
            'UncertaintyBands:List = {isrRedHi isr:muRfac=0.707,fsrRedHi fsr:muRfac=0.707,isrRedLo isr:muRfac=1.414,fsrRedLo fsr:muRfac=1.414,isrDefHi isr:muRfac=0.5,fsrDefHi fsr:muRfac=0.5,isrDefLo isr:muRfac=2.0,fsrDefLo fsr:muRfac=2.0,isrConHi isr:muRfac=0.25,fsrConHi fsr:muRfac=0.25,isrConLo isr:muRfac=4.0,fsrConLo fsr:muRfac=4.0,fsr_G2GG_muR_dn fsr:G2GG:muRfac=0.5,fsr_G2GG_muR_up fsr:G2GG:muRfac=2.0,fsr_G2QQ_muR_dn fsr:G2QQ:muRfac=0.5,fsr_G2QQ_muR_up fsr:G2QQ:muRfac=2.0,fsr_Q2QG_muR_dn fsr:Q2QG:muRfac=0.5,fsr_Q2QG_muR_up fsr:Q2QG:muRfac=2.0,fsr_X2XG_muR_dn fsr:X2XG:muRfac=0.5,fsr_X2XG_muR_up fsr:X2XG:muRfac=2.0,fsr_G2GG_cNS_dn fsr:G2GG:cNS=-2.0,fsr_G2GG_cNS_up fsr:G2GG:cNS=2.0,fsr_G2QQ_cNS_dn fsr:G2QQ:cNS=-2.0,fsr_G2QQ_cNS_up fsr:G2QQ:cNS=2.0,fsr_Q2QG_cNS_dn fsr:Q2QG:cNS=-2.0,fsr_Q2QG_cNS_up fsr:Q2QG:cNS=2.0,fsr_X2XG_cNS_dn fsr:X2XG:cNS=-2.0,fsr_X2XG_cNS_up fsr:X2XG:cNS=2.0,isr_G2GG_muR_dn isr:G2GG:muRfac=0.5,isr_G2GG_muR_up isr:G2GG:muRfac=2.0,isr_G2QQ_muR_dn isr:G2QQ:muRfac=0.5,isr_G2QQ_muR_up isr:G2QQ:muRfac=2.0,isr_Q2QG_muR_dn isr:Q2QG:muRfac=0.5,isr_Q2QG_muR_up isr:Q2QG:muRfac=2.0,isr_X2XG_muR_dn isr:X2XG:muRfac=0.5,isr_X2XG_muR_up isr:X2XG:muRfac=2.0,isr_G2GG_cNS_dn isr:G2GG:cNS=-2.0,isr_G2GG_cNS_up isr:G2GG:cNS=2.0,isr_G2QQ_cNS_dn isr:G2QQ:cNS=-2.0,isr_G2QQ_cNS_up isr:G2QQ:cNS=2.0,isr_Q2QG_cNS_dn isr:Q2QG:cNS=-2.0,isr_Q2QG_cNS_up isr:Q2QG:cNS=2.0,isr_X2XG_cNS_dn isr:X2XG:cNS=-2.0,isr_X2XG_cNS_up isr:X2XG:cNS=2.0}', 
            'UncertaintyBands:nFlavQ = 4', 
            'UncertaintyBands:MPIshowers = on', 
            'UncertaintyBands:overSampleFSR = 10.0', 
            'UncertaintyBands:overSampleISR = 10.0', 
            'UncertaintyBands:FSRpTmin2Fac = 20', 
            'UncertaintyBands:ISRpTmin2Fac = 1')
    ),
    comEnergy = cms.double(13000.0),
    crossSection = cms.untracked.double(1.0),
    filterEfficiency = cms.untracked.double(1.0),
    maxEventsToPrint = cms.untracked.int32(0),
    pythiaHepMCVerbosity = cms.untracked.bool(True),
    pythiaPylistVerbosity = cms.untracked.int32(True),
    reweightGenEmp = cms.PSet(
        tune = cms.string('CP5')
    )
)


process.ProductionFilterSequence = cms.Sequence(process.generator+process.pho_filter+process.gj_filter)

# Path and EndPath definitions
process.generation_step = cms.Path(process.pgen)
process.simulation_step = cms.Path(process.psim)
process.digitisation_step = cms.Path(process.pdigi)
process.datamixing_step = cms.Path(process.pdatamix)
process.L1simulation_step = cms.Path(process.SimL1Emulator)
process.digi2raw_step = cms.Path(process.DigiToRaw)
process.genfiltersummary_step = cms.EndPath(process.genFilterSummary)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RAWSIMoutput_step = cms.EndPath(process.RAWSIMoutput)

# Schedule definition
process.schedule = cms.Schedule(process.generation_step,process.genfiltersummary_step,process.simulation_step,process.digitisation_step,process.datamixing_step,process.L1simulation_step,process.digi2raw_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.extend([process.endjob_step,process.RAWSIMoutput_step])
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

#Setup FWK for multithreaded
process.options.numberOfThreads=cms.untracked.uint32(4)
process.options.numberOfStreams=cms.untracked.uint32(0)
# filter all path with the production filter sequence
for path in process.paths:
	getattr(process,path)._seq = process.ProductionFilterSequence * getattr(process,path)._seq 

# customisation of the process.

# Automatic addition of the customisation function from Configuration.DataProcessing.Utils
from Configuration.DataProcessing.Utils import addMonitoring 

#call to customisation function addMonitoring imported from Configuration.DataProcessing.Utils
process = addMonitoring(process)

# Automatic addition of the customisation function from HLTrigger.Configuration.customizeHLTforMC
from HLTrigger.Configuration.customizeHLTforMC import customizeHLTforMC 

#call to customisation function customizeHLTforMC imported from HLTrigger.Configuration.customizeHLTforMC
process = customizeHLTforMC(process)

# End of customisation functions

# Customisation from command line

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
